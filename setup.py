from setuptools import setup


setup(
      name="Lynch",
      version="1.0.0",
      packages=["lynch"],
      url="https://bitbucket.org/protocypher/ma-lynch",
      license="MIT",
      author="Benjamin Gates",
      author_email="benjamin@snowmantheater.com",
      description="A text based hangman game."
)

