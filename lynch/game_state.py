from enum import Enum


class GameState(Enum):
    PLAYING = 1
    VICTORY = 2
    FAILURE = 3
