class DuplicateException(Exception):
    def __init__(self, letter):
        super().__init__(f"Already guessed: {letter}")


class GameStateException(Exception):
    def __init__(self, state):
        super().__init__(f"Invalid game state: {state}")
