from .exceptions import DuplicateException, GameStateException
from .game_state import GameState
import random


class Hangman:
    MAX_GUESSES = 6
    HINT_BLANK = "_"
    HINT_SEP = " "

    def __init__(self):
        with open("dat/words.txt", mode="r") as w:
            words = w.readlines()
            self._word = random.choice(words).strip()

        self._letters = set(self._word)
        self._guessed = set()
        self._state = GameState.PLAYING

    @property
    def word(self):
        return self._word

    @property
    def letters(self):
        return self._letters

    @property
    def guessed(self):
        return self._guessed

    @property
    def hint(self):
        h = []

        for letter in self._word:
            h.append(letter if letter in self._guessed else Hangman.HINT_BLANK)

        return Hangman.HINT_SEP.join(h)

    @property
    def guesses(self):
        return len(self._guessed - self._letters)

    @property
    def state(self):
        return self._state

    def guess(self, letter):
        if self._state is not GameState.PLAYING:
            raise GameStateException(self._state)

        if letter.upper() in self._guessed:
            raise DuplicateException(letter)

        self._guessed.add(letter.upper())

        if self._guessed >= self._letters:
            self._state = GameState.VICTORY
        elif self.guesses > Hangman.MAX_GUESSES:
            self._state = GameState.FAILURE

        return self._state
