from colorama import Fore, Style

COLOR = {
    "@!": Style.RESET_ALL,
    "@w": Fore.WHITE + Style.DIM,   "@W": Fore.WHITE + Style.BRIGHT,
    "@k": Fore.BLACK + Style.DIM,   "@K": Fore.BLACK + Style.BRIGHT,
    "@b": Fore.BLUE + Style.DIM,    "@B": Fore.BLUE + Style.BRIGHT,
    "@c": Fore.CYAN + Style.DIM,    "@C": Fore.CYAN + Style.BRIGHT,
    "@g": Fore.GREEN + Style.DIM,   "@G": Fore.GREEN + Style.BRIGHT,
    "@y": Fore.YELLOW + Style.DIM,  "@Y": Fore.YELLOW + Style.BRIGHT,
    "@r": Fore.RED + Style.DIM,     "@R": Fore.RED + Style.BRIGHT,
    "@m": Fore.MAGENTA + Style.DIM, "@M": Fore.MAGENTA + Style.BRIGHT,
}

NOCOLOR = {
    "@!": "", "@w": "", "@W": "", "@k": "", "@K": "",
    "@b": "", "@B": "", "@c": "", "@C": "", "@g": "", "@G": "",
    "@y": "", "@Y": "", "@r": "", "@R": "", "@m": "", "@M": "",
}
