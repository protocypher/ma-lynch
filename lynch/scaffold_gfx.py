class ScaffoldGfx:
    def __init__(self):
        self._frames = []

        with open("dat/scaffold.gfx", mode="r") as gfx:
            lines = gfx.readlines()

            for i in range(0, 42, 6):
                self._frames.append("\n".join(map(str.rstrip, lines[i:i+6])))

    def __getitem__(self, i):
        return self._frames[i]
