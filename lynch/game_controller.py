from cmd import Cmd
from .game_state import GameState
from .hangman import Hangman
from .exceptions import DuplicateException, GameStateException
from .game_view import GameView


class GameController(Cmd):
    def __init__(self):
        super().__init__()

        self.game = None
        self.view = GameView()
        self.setup_theme("nocolor")

    def setup_theme(self, theme):
        try:
            self.view.theme = theme

            self.prompt = self.view.prompt
            self.intro = self.view.intro
            self.doc_header = self.view.header
            self.ruler = self.view.ruler
        except ValueError:
            print(self.view.error(f"{theme} is not a valid them; see `themes`."))

    def default(self, arg):
        if self.game and len(arg) == 1:
            self.do_guess(arg)
        else:
            print(self.view.error(f"Unrecognized command `{arg}`."))

    def emptyline(self):
        if self.game is not None:
            print(self.view.game_state(self.game.hint, self.game.guesses))
        else:
            print(self.intro)

    def help_new(self):
        print(self.view.command_help(
             "NEW",
             "new",
             "Starts a new game (aborting any existing game)."
        ))

    def do_new(self, arg):
        self.view.success("Beginning new game!")
        self.game = Hangman()
        print(self.view.game_state(self.game.hint, self.game.guesses))

    def help_guess(self):
        print(self.view.command_help(
             "GUESS",
             "guess <letter>",
             "Guesses `letter` in the current game."
        ))

    def do_guess(self, arg):
        if not self.game:
            print(self.view.error("Please start a new game before guessing."))
            return
        elif not arg:
            print(self.view.error("You cannot guess `no letter`."))
            return
        elif len(arg) > 1:
            print(self.view.error("You can only guess one letter at a time."))
            return
        elif not arg.isalpha():
            print(self.view.error("You can only guess letters."))

        try:
            self.game.guess(arg)
        except DuplicateException:
            print(self.view.warn("You already guessed that letter."))
        except GameStateException:
            print(self.view.syserr("Unexpected game state; halting application."))
            return True

        if self.game.state is GameState.VICTORY:
            print(self.view.success("Congratz! You won!"))
            print(self.view.info(f"The word was {self.game.word}."))
            self.game = None
            return

        if self.game.state is GameState.FAILURE:
            print(self.view.error("Too bad, you lost!"))
            print(self.view.info(f"The word was {self.game.word}."))
            self.game = None
            return

        print(self.view.game_state(self.game.hint, self.game.guesses))

    def help_theme(self):
        print(self.view.command_help(
             "THEME",
             "theme [<theme>]",
             "Sets or shows the view's current theme."
        ))

    def do_theme(self, arg):
        if not arg:
            print(self.view.info(f"The current theme is: {self.view.theme}"))
            return

        self.setup_theme(arg)

    def help_themes(self):
        print(self.view.command_help(
             "THEMES",
             "themes",
             "Lists the available themes to use with `theme`."
        ))

    def do_themes(self, arg):
        print(self.view.info("Available themes are (" + ", ".join(self.view.themes) + ")."))

    def help_quit(self):
        print(self.view.command_help(
             "QUIT",
             "quit",
             "Quits the appllication."
        ))

    def do_quit(self, arg):
        print(self.view.info("Good bye; thanks for playing"))
        return True
