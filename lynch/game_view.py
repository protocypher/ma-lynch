from .scaffold_gfx import ScaffoldGfx
from colorama import deinit, init
from .themes import COLOR, NOCOLOR


class GameView:
    _PROMPT = "\n@C> @!"
    _INTRO = "\n@CWelcom to Lynch; a civil game of hangman. @c(@Y?@c for help)@!"
    _HEADER = "\n@CCommands @c(@Y? <command>@c for help on a command)@!"
    _RULER = "@B=@!"

    def __init__(self):
        self._gfx = ScaffoldGfx()
        self.theme = "nocolor"

    def apply_theme(self, text):
        for code, ansi in self._theme.items():
            text = text.replace(code, ansi)

        return text

    @property
    def prompt(self):
        return self.apply_theme(GameView._PROMPT)

    @property
    def intro(self):
        return self.apply_theme(GameView._INTRO)

    @property
    def header(self):
        return self.apply_theme(GameView._HEADER)

    @property
    def ruler(self):
        return self.apply_theme(GameView._RULER)

    @property
    def theme(self):
        return "color" if self._theme is COLOR else "nocolor"

    @theme.setter
    def theme(self, requested):
        if requested == "color":
            self._theme = COLOR
            init()
        elif requested == "nocolor":
            self._theme = NOCOLOR
            deinit()
        else:
            raise ValueError(f"No such theme {requested}.")

    @property
    def themes(self):
        return ["color", "nocolor"]

    def success(self, text):
        return self.apply_theme(f"\n@G{text}@!")

    def info(self, text):
        return self.apply_theme(f"\n@W{text}@!")

    def warn(self, text):
        return self.apply_theme(f"\n@Y{text}@!")

    def error(self, text):
        return self.apply_theme(f"\n@R{text}@!")

    def syserr(self, text):
        return self.apply_theme(f"\n@M{text}@!")

    def command_help(self, label, example, description):
        hlp = self.apply_theme(f"\n@B<<< @C{label} @B>>>")
        hlp += self.apply_theme(f"\n@G{example}")
        hlp += self.apply_theme(f"\n@W{description}@!")
        return self.apply_theme(hlp)

    def game_state(self, hint, guesses):
        state = f"\n@B{self._gfx[guesses]}"
        state += f"\n@WHint: @C{hint}@!"
        return self.apply_theme(state)
