# Lynch

Lynch is a basic, text based, hangman game that uses the `cmd.Cmd` framework.
It also uses `colorama` for a smattering of color to make things a little more
cheery.

